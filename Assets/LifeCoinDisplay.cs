﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class LifeCoinDisplay : MonoBehaviour
{
    public LifeInfo model;

    Image image;
    TextMeshProUGUI countText;

    // Start is called before the first frame update
    void Awake()
    {
        image = GetComponent<Image>();
        countText = GetComponentInChildren<TextMeshProUGUI>();
    }
    private void Start()
    {
        image.sprite = model.life.sprite;
        countText.text = "x" + model.count;
    }
}
