using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Story
{
    [SerializeField]
    public string title;
    [SerializeField, TextArea]
    public List<string> texts;
    public LifeInfo reward;
    public AudioClip audio;
}

[System.Serializable]
public class StoryInfo
{
    [SerializeField]
    public Story story;
    [Range(1,100)]
    public int weightChance = 1;
}

[CreateAssetMenu(fileName = "Life"), System.Serializable]
public class Life : ScriptableObject
{
    public string title;
    public Sprite sprite;
    public Color color;

    [SerializeField]
    public List<StoryInfo> stories;

    public StoryInfo GetRandomStoryInfo()
    {
        if (stories.Count <= 0)
        {
            // Should not happen.
        }
        int totalWeights = 0;
        foreach (StoryInfo storyInfo in stories) {
            totalWeights += storyInfo.weightChance;
        }
        int randomChance = Random.Range(0, totalWeights);

        totalWeights = 0;
        foreach (StoryInfo storyInfo in stories)
        {
            totalWeights += storyInfo.weightChance;
            if (randomChance < totalWeights)
            {
                return storyInfo;
            }
        }
        return stories[stories.Count - 1];
    }
}
