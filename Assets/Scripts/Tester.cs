﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{

    [SerializeField]
    RewardManager rewardManager;

    [SerializeField]
    Life life;


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            rewardManager.PlayReward(life.stories[Random.Range(0, life.stories.Count)]);
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            rewardManager.StopAnimation();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            LifeInventory.Instance.ClearSave();
        }
    }
}
