﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChoiceManager : HidableCanvasGroup
{
    [SerializeField]
    ChoiceButtonBehavior LeftBtn, RightBtn;
    
    [SerializeField]
    StoryManager storyManager;

    [SerializeField]
    NoChoiceManager noChoiceManager;

    LifeFiller lifeFiller;

    List<LifeInfo> currentLifeChoices = new List<LifeInfo>();
    
    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        Reload();
    }

    private void Awake()
    {
        lifeFiller = GetComponentInChildren<LifeFiller>();
    }

    void SetChoices(LifeInfo left, LifeInfo right)
    {
        Show();
        currentLifeChoices = new List<LifeInfo>();
        currentLifeChoices.Add(left);
        currentLifeChoices.Add(right);
        LeftBtn.SetChoice(left);
        RightBtn.SetChoice(right);
        lifeFiller.ReloadCells();
    }

    internal void PlayChoice(LifeInfo info)
    {
        Hide(0.01f);
        storyManager.PlayLife(info);

        foreach (var lifeinfo in currentLifeChoices)
        {
            LifeInventory.Instance.spendLifeInfo(lifeinfo);
        }
        lifeFiller.ReloadCells();
    }

    public void Reload()
    {
        var infos = LifeInventory.Instance.GetLivesInfo(2);
        if (infos.Count == 0)
        {
            LifeInventory.Instance.ClearSave();
            SceneManager.LoadScene((int)SceneEnum.GameOver);
            return;
        }
        if (infos.Count == 1)
        {
            Hide();
            noChoiceManager.SetChoice(infos[0]);
        }
        else
        {
            SetChoices(infos[0], infos[1]);
        }
    }

}
