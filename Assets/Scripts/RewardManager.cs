﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardManager : HidableCanvasGroup
{
    [SerializeField]
    protected ChoiceManager choiceManager;

    [SerializeField]
    RewardsAnimation rewardItemPrefab;

    [SerializeField]
    Transform rewardItemParent;

    [SerializeField]
    TextMeshProUGUI title;

    RewardsAnimation currentRewardItem;


    protected TextSpawner textSpawner;

    private void Awake()
    {
        textSpawner = GetComponentInChildren<TextSpawner>();
        canvasGroup = GetComponent<CanvasGroup>();    
    }

    internal void PlayReward(StoryInfo storyInfo)
    {
        if (storyInfo.story.reward.life == null || storyInfo.story.reward.count == 0)
        {
            choiceManager.Reload();
            return;
        }
        Show();
        title.text = "Earned \"" + storyInfo.story.reward.life.name + "\" life";
        Debug.Log("won " + storyInfo.story.reward.life.name);

        currentRewardItem = Instantiate(rewardItemPrefab, rewardItemParent);
        currentRewardItem.gameObject.SetActive(false);
        currentRewardItem.SetIcon(storyInfo);
        currentRewardItem.gameObject.SetActive(true);

        /* on fait des anim on get le reward*/

        LifeInventory.Instance.AddLives(storyInfo.story.reward);
        textSpawner.StartSpawning(storyInfo.story.reward.life.name);
    }

    public void StopAnimation()
    {
        textSpawner.StopSpawning();
        if (currentRewardItem.gameObject)
        {
            Destroy(currentRewardItem.gameObject);
        }
    }

    public void Reborn()
    {
        StopAnimation();
        Hide();
        choiceManager.Reload();
    }
}
