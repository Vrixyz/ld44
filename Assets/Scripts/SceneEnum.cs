﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Flags]
public enum SceneEnum
{
    Menu = 0,
    Game = 1,
    GameOver = 2
}