﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup), typeof(RectTransform))]
public class LifeFiller : MonoBehaviour
{
    [SerializeField]
    LifeCoinDisplay coinDisplayPrefab;
    [SerializeField]
    int numberOfLines = 2;

    GridLayoutGroup gridLayoutGroup;
    RectTransform rectTransform;


        // Start is called before the first frame update
void Start()
    {
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        rectTransform = GetComponent<RectTransform>();
    }
    public void ReloadCells()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
        var bounds = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
        // Cell is square
        var cellSize = bounds.y / numberOfLines;
        var numberOfColumns = bounds.x / cellSize;
        gridLayoutGroup.cellSize = new Vector3(cellSize, cellSize, cellSize);
        var lives = LifeInventory.Instance.currentLives;

        foreach (var life in lives)
        {
            var coinDisplay = Instantiate(coinDisplayPrefab, transform);
            coinDisplay.model = life;
        }
    }
}
