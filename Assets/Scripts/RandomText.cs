﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class RandomText : MonoBehaviour
{
    [SerializeField]
    List<string> texts;

    TextMeshProUGUI textMeshPro;

    private void Awake()
    {
        textMeshPro = GetComponent<TextMeshProUGUI>();
        if (texts == null || texts.Count == 0)
        {
            texts = new List<string>();
            texts.Add(textMeshPro.text);
        }
    }

    private void OnEnable()
    {
        if (texts.Count > 0)
        {
            this.textMeshPro.text = texts[Random.Range(0, texts.Count)];
        }
    }
}
