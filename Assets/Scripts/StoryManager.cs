using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource), typeof(Animator))]
public class StoryManager : HidableCanvasGroup
{
    [SerializeField]
    protected RewardManager rewardManager;
    [SerializeField]
    private Text textField;
    [SerializeField]
    private Button continueBtn;

    private StoryInfo _storyInfo;
    private int _textIdx;
    private AudioSource _audioSource;

    private Animator animator;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        animator = GetComponent<Animator>();
    }

    internal void PlayLife(LifeInfo lifeInfo)
    {
        /* CHOOSE STORY */
        var storyInfo = lifeInfo.life.GetRandomStoryInfo();
        PlayStory(storyInfo);
    }

    internal void PlayStory(StoryInfo storyInfo)
    {
        _storyInfo = storyInfo;
        
        Show(0.01f);
        animator.Play("StoryAnimation");
        if (_audioSource == null)
        {
            _audioSource = GetComponent<AudioSource>();
        }
        _audioSource.Stop();
        if (_storyInfo.story.audio)
        {
            _audioSource.clip = _storyInfo.story.audio;
            _audioSource.Play();
        }
        _textIdx = 0;
        continueBtn.GetComponentInChildren<Text>().text = "Continue Story";
        ContinueStory();
    }

    public void ContinueStory()
    {
        if (_storyInfo.story.texts.Count == _textIdx + 1)
        {
            continueBtn.GetComponentInChildren<Text>().text = "End this life.";
        }
        
        if (_storyInfo.story.texts.Count > _textIdx)
        {
            textField.text = _storyInfo.story.texts[_textIdx];
        }
        else
        {
            GetReward();
        }
        _textIdx++;
    }

    public void GetReward()
    {
        Hide();

        //Go to choice mode
        rewardManager.PlayReward(_storyInfo);
    }
}
