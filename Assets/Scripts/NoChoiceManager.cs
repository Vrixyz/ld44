﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoChoiceManager : HidableCanvasGroup
{
    [SerializeField]
    ChoiceButtonBehavior Btn;

    [SerializeField]
    StoryManager storyManager;

    LifeFiller lifeFiller;
    // Start is called before the first frame update
    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        lifeFiller = GetComponentInChildren<LifeFiller>();
    }

    internal void PlayChoice(LifeInfo lifeInfo)
    {
        Hide(0.01f);
        storyManager.PlayLife(lifeInfo);
        LifeInventory.Instance.spendLifeInfo(lifeInfo);
        lifeFiller.ReloadCells();
    }

    internal void SetChoice(LifeInfo lifeInfo)
    {
        Btn.SetChoice(lifeInfo);
        Show();
        lifeFiller.ReloadCells();
    }
}
