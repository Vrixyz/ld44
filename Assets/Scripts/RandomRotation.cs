﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    [SerializeField]
    float rotationAmplitude = 10f;

    // Start is called before the first frame update
    void Start()
    {
        var rotation = Random.value * rotationAmplitude - rotationAmplitude / 2;
        this.transform.eulerAngles = new Vector3(0, 0, rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
