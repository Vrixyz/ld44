﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSpawner : MonoBehaviour
{
    [SerializeField]
    float spawnPerSeconds = 10f;
    [SerializeField]
    GameObject prefab;

    
    string textToDisplay = "Engineer";

    bool isSpawning = false;

    // Start is called before the first frame update
    public void StartSpawning(string textToSpawn)
    {
        this.isSpawning = true;
        textToDisplay = textToSpawn;
        StartCoroutine(SpawnTexts());
    }

    public void StopSpawning()
    {
        this.isSpawning = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    IEnumerator SpawnTexts()
    {
        while (this.isSpawning)
        {
            var newText = Instantiate(prefab);
            var textmesh = newText.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            if (textmesh)
            {
                textmesh.text = this.textToDisplay;
            }
            newText.transform.SetParent(this.transform);
            newText.transform.position = new Vector3(0, 0, 0);
            newText.transform.localScale = Vector3.one;
            newText.transform.localPosition = new Vector3(Random.value * Screen.width - Screen.width / 2, Random.value * Screen.height - Screen.height / 2, 0);
            yield return new WaitForSeconds(1 / spawnPerSeconds);
        }
    }
}
