using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChoiceButtonBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    NoChoiceManager noChoiceManager;
    [SerializeField]
    private ChoiceManager choiceManager;
    [SerializeField]
    private GameObject spritePlace;
    [SerializeField]
    private Text btnText;

    [SerializeField]
    private GameObject hintOnSelect;

    private LifeInfo _info;

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void SetChoice(LifeInfo info)
    {
        if (info.life.sprite == null 
            || info.life.title == null
            || info.life.color == null)
        {
            Debug.LogError("Param missing in Life:" + info.life.name);
            return;
        }

        DestroyImmediate(spritePlace.GetComponent<Image>());
        
        var image = spritePlace.gameObject.AddComponent<Image>();
        image.sprite = info.life.sprite;
        image.preserveAspect = true;
        image.color = info.life.color;
        btnText.text = info.life.title;

        _info = info;
    }

    public void SelectLife()
    {
        if (choiceManager != null)
        {
            choiceManager.DisableInteraction();
            choiceManager.PlayChoice(_info);
        }
        else
        {
            noChoiceManager.DisableInteraction();
            noChoiceManager.PlayChoice(_info);
        }
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (hintOnSelect)
            hintOnSelect.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        if (hintOnSelect)
            hintOnSelect.gameObject.SetActive(false);
    }
}
