﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptToCanvaSize : MonoBehaviour
{
    [SerializeField]
    Canvas canvas;

    // Update is called once per frame
    void Update()
    {
        var rectTransform = canvas.GetComponent<RectTransform>();
        var width = rectTransform.rect.width;
        var height = rectTransform.rect.height;

        var max = height > width ? height : width;

        var rect = gameObject.GetComponent<RectTransform>().rect;
        if(rect.height != max)
        {
            rect.height = max;
            rect.width = max;
        }
    }
}
