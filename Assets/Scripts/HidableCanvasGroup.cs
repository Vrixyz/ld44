﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HidableCanvasGroup : MonoBehaviour
{
    [SerializeField]
    protected CanvasGroup canvasGroup;

    public void Hide(float animationSpeed = 0.1f)
    {
        StartCoroutine(HideAnimation(animationSpeed));
    }

    private IEnumerator HideAnimation(float animationSpeed)
    {
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        for (; canvasGroup.alpha > 0; canvasGroup.alpha-= animationSpeed)
        {
            yield return new WaitForSeconds(0.01f);
        }
    }

    private IEnumerator ShowAnimation(float animationSpeed)
    {
        for (; canvasGroup.alpha < 1; canvasGroup.alpha += animationSpeed)
        {
            yield return new WaitForSeconds(0.01f);
        }
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

    public void Show(float animationSpeed = 0.1f)
    {
        StartCoroutine(ShowAnimation(animationSpeed));
    }

    public void DisableInteraction()
    {
        canvasGroup.interactable = false;
    }
}
