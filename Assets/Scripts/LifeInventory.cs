using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Random = UnityEngine.Random;


[Serializable]
public struct LifeInfo
{
    public Life life;
    public uint count;
}

public class LifeInventory : MonoBehaviour
{
    [SerializeField]
    protected bool overrideSave;

    private LifeInventory() { }
    [SerializeField]
    protected List<LifeInfo> lives;


    [SerializeField]
    public List<LifeInfo> currentLives { get; private set; }

    private string saveFilepath = "/savedGame.gd";
    private ScoreManager _scoreManager;

    public static LifeInventory Instance { get; private set; }

    internal void AddLives(LifeInfo reward)
    {
        if (_scoreManager != null)
        {
            _scoreManager.AddScore((int)reward.count);
        }
        for (int i = 0; i < currentLives.Count; i++)
        {
            var currentlife = currentLives[i];
            if (currentlife.life.Equals(reward.life))
            {
                currentlife.count += reward.count;
                currentLives[i] = currentlife;
                return;
            }
        }

        currentLives.Add(reward);
        this.Save();
    }

    void Awake()
    {
        if (Instance != null && Instance != this) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        this.Load();
        DontDestroyOnLoad(this.gameObject);
    }
    
    public List<LifeInfo> GetLivesInfo(int count)
    {
        if (count > currentLives.Count)
        {
            return currentLives;
        }

        List<LifeInfo> livesToReturn = new List<LifeInfo>();

        int randomIndex = -1;
        for (int i = 0; i < count; i++)
        {
            int previous = randomIndex;
            while (randomIndex == previous)
            {
                randomIndex = Random.Range(0, currentLives.Count);
            }
            livesToReturn.Add(currentLives[randomIndex]);
        }
        return livesToReturn;
    }

    /// <returns><c>true</c>, if life was spent, <c>false</c> otherwise.</returns>
    /// <param name="lifeInfo">Life info.</param>
    public bool spendLifeInfo(LifeInfo lifeInfo)
    {
        for (int i = 0; i < currentLives.Count; i++)
        {
            var currentlife = currentLives[i];
            if (currentlife.Equals(lifeInfo))
            {
                if (currentlife.count <= 0)
                {
                    return false;
                }

                currentlife.count --;
                currentLives[i] = currentlife;
                if (currentlife.count <= 0)
                {
                    currentLives.RemoveAt(i);
                }
                Save();
                return true;
            }
        }
        return false;
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        var saveableLives = new Dictionary<string, long>();
        foreach (LifeInfo info in currentLives) {
            saveableLives[info.life.name] = info.count;
        }

        FileStream file = File.Create(Application.persistentDataPath + saveFilepath);
        bf.Serialize(file, saveableLives);

        file.Close();
    }

    public void Load()
    {
        if (overrideSave)
        {
            currentLives = new List<LifeInfo>(lives);
            return;
        }
        if (File.Exists(Application.persistentDataPath + saveFilepath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + saveFilepath, FileMode.Open);
            var savedLives = (Dictionary<string, long>)bf.Deserialize(file);
            if (savedLives == null)
            {
                currentLives = new List<LifeInfo>(lives);
                return;
            }
            var loadedLives = new List<LifeInfo>();

            foreach (KeyValuePair<string, long> savedLife in savedLives)
            {
                var life = Resources.Load<Life>("ScriptableObjects/Lives/" + savedLife.Key);
                if (life != null)
                {
                    var lifeInfo = new LifeInfo();
                    lifeInfo.count = (uint)savedLife.Value;
                    lifeInfo.life = life;
                    loadedLives.Add(lifeInfo);
                }
            }
            currentLives = loadedLives;
            file.Close();
        }
        else
        {
            currentLives = new List<LifeInfo>(lives);
        }
    }

    public void ClearSave()
    {
        File.Delete(Application.persistentDataPath + saveFilepath);
        currentLives = new List<LifeInfo>(lives);
    }
    
    internal void SetScoreManager(ScoreManager scoreManager)
    {
        _scoreManager = scoreManager;
    }
}
