﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using TMPro;

public class RewardsAnimation : MonoBehaviour
{
    [SerializeField]
    protected Image icone;

    [SerializeField]
    protected TextMeshProUGUI countTxt;

    [HideInInspector]
    public StoryInfo storyInfo;

    public void SetIcon(StoryInfo _storyInfo)
    {
        this.storyInfo = _storyInfo;
        icone.sprite = storyInfo.story.reward.life.sprite;
        countTxt.text = "x" + storyInfo.story.reward.count;
    }
}
