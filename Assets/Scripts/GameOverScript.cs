﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverScript : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        var score = PlayerPrefs.GetInt("Score", 0);
        var scoreMax = PlayerPrefs.GetInt("MaxScore", 0);
        text.text = "Your Final Score (Souls) is " + score;
        if (score > scoreMax)
        {
            PlayerPrefs.SetInt("MaxScore", score);
        }
        PlayerPrefs.SetInt("Score", 0);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene((int)SceneEnum.Game);
    }
}
