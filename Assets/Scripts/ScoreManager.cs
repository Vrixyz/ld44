﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private List<TextMeshProUGUI> texts;

    private int _score = 0;

    public void Start()
    {
        LifeInventory.Instance.SetScoreManager(this);
        var score = PlayerPrefs.GetInt("Score", 0);
        AddScore(score);
    }

    public void AddScore(int score)
    {
        _score += score;
        PlayerPrefs.SetInt("Score", _score);
        foreach(var text in texts)
        {
            text.text = "Souls : " + _score;
        }
    }
}
